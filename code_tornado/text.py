"""Handle input text, building into a master list with blocks."""
# Code Tornado: A line-by-line text randomization tool. See LICENSE.md for full license.
# Copyright (C) 2023 Grant Ward
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import copy
import re
import secrets


COMMENT_STR = '#'
NAME_CHARS = '[a-zA-Z0-9_-]'
LINE_END = '\n'
DEFAULT_FILTERS = ['random']


class Block:
    """Text block to be populated by lines."""
    def __init__(self, name):
        self.name = name
        self.lines = list()

    def filter(self, filters):
        """Apply filters to a copy of text and return."""
        lines = copy.deepcopy(self.lines)
        for name in filters:
            lines = getattr(self, f'filter_{name}')(lines)
        return lines

    def add_line(self, line):
        """Insert a line into this block."""
        self.lines.append(line)

    def dumps(self, filters=None):
        """Dump lines into a single string, applying requested filters."""
        lines = self.filter(filters) if filters else self.lines.copy()
        return LINE_END.join(lines) + LINE_END

    @staticmethod
    def filter_random(lines):
        """Randomize lines in a cryptographically secure fashion."""
        output_lines = []
        while lines:
            output_lines.append(random_pop(lines))
        return output_lines


def build_master_list(input_files, config):
    """Build a master list with text lines and blocks in order."""
    comment_str = config.get('comment_str', COMMENT_STR)
    # Regex to create a new block at specified location
    block_create_re = re.compile(
        rf'\s*{comment_str}\s*tornado:\s*block:\s*(?P<name>{NAME_CHARS}+)\s*')
    # Regex to place a line in a block (random internal position)
    block_place_re = re.compile(
        rf'{comment_str}\s*tornado:\s*place-in-block:\s*(?P<name>{NAME_CHARS}+)\s*$')
    # The master list to return, contains lines (strings) and blocks, ordered
    built = list()
    # Index of blocks within the list
    blocks = dict()

    for input_file in input_files:
        for idx, line in enumerate(input_file.readlines()):
            if matched := block_create_re.fullmatch(line):
                # Generate a new block to add lines to
                name = matched.group('name')
                built.append(Block(name=name))
                if blocks.get(name):
                    # duplicate block, error
                    raise ValueError(
                        f'In file {input_file.name}: Line {idx}: Duplicate block ({name}) created. '
                        'Block names must be unique.'
                    )
                blocks[name] = built[-1]
            elif matched := block_place_re.search(line):
                # Add line to an existing block
                name = matched.group('name')
                block = blocks.get(name)
                if not block:
                    raise ValueError(
                        f'In file {input_file.name}: Line {idx}: Block ({name}) does not exist.'
                    )
                # Add the entire line up until the tornado command
                block.add_line(line[0 : len(line)-len(matched.group(0))])
            else:
                built.append(line)

    return built


def dump_master_list(stream, master_list, config):
    """Dump the text within a master list to a stream."""
    filters = config.get('filters', DEFAULT_FILTERS)
    actions = {
        Block: lambda block: block.dumps(filters=filters),
    }
    for line_or_block in master_list:
        stream.write(actions.get(line_or_block.__class__, lambda obj: obj)(line_or_block))


def random_pop(sequence):
    """Select an element from sequence at random, and pop it."""
    idx = secrets.randbelow(len(sequence))
    return sequence.pop(idx)
