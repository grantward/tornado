#!/usr/bin/env python3
"""CLI entrypoint into Tornado."""
# Code Tornado: A line-by-line text randomization tool. See LICENSE.md for full license.
# Copyright (C) 2023 Grant Ward
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import sys

from . import text


TEXT_ENCODING='utf-8'


def tornado_cli(argv, close_output=True):
    """Entrypoint into Tornado."""
    parser = argparse.ArgumentParser(
        prog='Tornado',
        description='Cryptographically secure text line ordering randomization',
    )
    parser.add_argument(
        'input_files',
        type=lambda filename: open(filename, 'r', encoding=TEXT_ENCODING),
        nargs='*',
        default=[sys.stdin],
        help='Files to obtain text from, if multiple will be cat\'d',
    )
    parser.add_argument(
        '-o',
        '--output-file',
        type=lambda filename: open(filename, 'w', encoding=TEXT_ENCODING),
        default=sys.stdout,
        help='File to output to. If not provided, stdout.',
    )

    try:
        args = parser.parse_args(argv)
    except FileNotFoundError as err:
        # Input files could not be opened
        print(err, file=sys.stderr)
        return 1
    open_files = []
    open_files.extend(args.input_files)
    if close_output:
        open_files.append(args.output_file)

    # Build config (settings)
    config = {}

    # Read lines into master list
    try:
        master_list = text.build_master_list(args.input_files, config=config)
    except ValueError as err:
        # Input files had errors, such as bad block names
        print(err, file=sys.stderr)
        cleanup_files(open_files)
        return 2

    # Output lines into output stream
    text.dump_master_list(args.output_file, master_list, config=config)

    # Success
    cleanup_files(open_files)
    return 0


def cleanup_files(open_files):
    """Close all open file handles before exiting."""
    for open_file in open_files:
        open_file.close()


if __name__ == '__main__':
    sys.exit(tornado_cli(sys.argv[1:]))
