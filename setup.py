"""Package metadata for Code Tornado."""


import setuptools


with open('README.md', 'r', encoding = 'utf-8') as fh:
    long_description = fh.read()

setuptools.setup(
    name = 'code-tornado',
    version = '0.0.1',
    python_requires = '>=3.6',
    author = 'Grant Ward',
    author_email = 'gw1@email.com',
    license = 'AGPL-3.0-only',
    description = 'Cryptographically secure line-by-line text order randomizer',
    long_description = long_description,
    long_description_content_type = 'text/markdown',
    url = 'package URL',
    project_urls = {
        'Source': 'https://gitlab.com/grantward/tornado',
        'Bug Tracker': 'https://gitlab.com/grantward/tornado/-/issues',
    },
    classifiers = [
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: GNU Affero General Public License version 3',
        'Operating System :: OS Independent',
    ],
    packages = ['code_tornado'],
)
