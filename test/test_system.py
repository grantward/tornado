"""Evaluate the tornado.text module."""
# pylint: disable=missing-function-docstring

import contextlib
import io
import unittest
import os
import re


from code_tornado import tornado #pylint: disable=import-error


class TestTornadoCli(unittest.TestCase):
    """Evaluate the CLI interface of Code Tornado"""
    TEST_FILE = os.path.join('examples', 'hello-world.py')
    with open(TEST_FILE, 'r', encoding='utf-8') as test_f:
        test_file_contents = test_f.read()

    def test_random_block(self):
        """Evaluate randomization of file input and output."""
        file_lines = self.test_file_contents.split('\n')
        # Strip block comments
        file_lines = [
            line for line in file_lines if not re.match(r'\s*#\s*tornado:\s*block:', line)]
        # Strip inline comments
        file_lines = [re.sub(r'\s*#\s*tornado:.*', ' ', line) for line in file_lines]

        different = False
        for _ in range(1, 100):
            # Surely the same output won't be randomly generated 100 times in a row
            stderr, stdout = run_tornado(self.TEST_FILE)
            # No error generated
            self.assertFalse(stderr)
            stdout_lines = stdout.split('\n')
            # Same lines were generated
            self.assertEqual(set(file_lines), set(stdout_lines))
            if file_lines != stdout_lines:
                # Random output different than input was generated
                different = True
                break
        self.assertTrue(different)


def run_tornado(input_file):
    """Runs Code Tornado on an input file (path), returning (stderr, stdout)."""
    with contextlib.redirect_stderr(io.StringIO()) as stderr_f:
        with contextlib.redirect_stdout(io.StringIO()) as stdout_f:
            tornado.tornado_cli([str(input_file)], close_output=False)
            stderr = stderr_f.getvalue()
            stdout = stdout_f.getvalue()
    return stderr, stdout


if __name__ == '__main__':
    unittest.main()
