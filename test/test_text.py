"""Evaluate the tornado.text module."""
# pylint: disable=missing-function-docstring

import unittest

from code_tornado import text #pylint: disable=import-error


class TestBlock(unittest.TestCase):
    """Evaluate the Block class within tornado.text"""
    LINES = [
        'aaa',
        'bbb',
        '123',
        'Hello World',
    ]
    DUMPED = 'aaa\nbbb\n123\nHello World\n'

    def setUp(self):
        self.block = text.Block(name='test_block')
        for line in self.LINES:
            self.block.add_line(line)

    def test_init(self):
        self.assertEqual(self.LINES, self.block.lines)

    def test_dumps(self):
        self.assertEqual(self.DUMPED, self.block.dumps())

    def test_random_dumps(self):
        random_dumps = [self.block.dumps(filters=['random']) for _ in range(0, 100)]
        matched = [self.DUMPED == random_dump for random_dump in random_dumps]
        contained = [set(self.DUMPED) == set(random_dump) for random_dump in random_dumps]
        # Surely we won't get 100 in a row all the same
        self.assertFalse(all(matched))
        self.assertTrue(all(contained))


if __name__ == '__main__':
    unittest.main()
