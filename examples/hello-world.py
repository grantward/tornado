#!/usr/bin/env python3
"""I am a simple program that prints hello world in a randomized order."""


def main():
    # tornado: block: hello-world
    print('h', end='') # tornado: place-in-block: hello-world
    print('e', end='') # tornado: place-in-block: hello-world
    print('l', end='') # tornado: place-in-block: hello-world
    print('l', end='') # tornado: place-in-block: hello-world
    print('o', end='') # tornado: place-in-block: hello-world
    print(' ', end='') # tornado: place-in-block: hello-world
    print('w', end='') # tornado: place-in-block: hello-world
    print('o', end='') # tornado: place-in-block: hello-world
    print('r', end='') # tornado: place-in-block: hello-world
    print('l', end='') # tornado: place-in-block: hello-world
    print('d', end='') # tornado: place-in-block: hello-world
    print()


if __name__ == '__main__':
    main()
