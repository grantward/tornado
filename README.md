# Code Tornado

## Description
A tool to randomize the order of specific lines of text. Specifically designed to be used for source code.
This program is licensed under the AGPLv3. See `LICENSE.md` for full information.

## Usage
To obtain the most up to date usage of the program, run the following:
```bash
./tornado.sh --help
```

## Examples
Examples are provided in the `examples` directory. To run the examples, do the following:

```bash
./tornado.sh examples/hello-world.py | python3
```
or to see the generated source code:
```bash
./tornado.sh examples/hello-world.py
```

## Tests
Tests are provided in the `test` directory. To run the tests, do the following:

```bash
./test/run-tests.sh
```
or to run a specific test:
```bash
python3 -m unittest test/test-text.py
```
